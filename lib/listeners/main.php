<?php
namespace Webformat\FormTransmitter\Listeners;
use \Bitrix\Main\Localization\Loc;

defined('B_PROLOG_INCLUDED') or die('no prolog in "'.basename(__FILE__).'"!');

Loc::loadMessages(__FILE__);

class Main{
    use Traits\Initable;

    protected static $moduleId = 'webformat.formtransmitter';

    public static function wfOnEpilog() {
		if(defined('ADMIN_SECTION') && \ADMIN_SECTION){return true;} //Выходим, если это админка
		self::init();
		$asset = \Bitrix\Main\Page\Asset::getInstance();

        $asset->addJs(self::$modulePath.'assets/js/injector.js', false);
        /* if((bool)\preg_match('#^\/workgroups\/group\/\d+\/calendar\/#', $_SERVER['REQUEST_URI'])){
            $path = self::$modulePath.'js/group_calendar_detail.js';
            $asset->addCss(self::$modulePath . 'css/deal_details.css');
        } */


        return true;
	}
}
