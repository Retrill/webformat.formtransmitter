<?php

namespace Webformat\FormTransmitter\Listeners;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Webformat\FormTransmitter\ConsumerStorage;
use Webformat\FormTransmitter\Http\BitrixSendKernel;
use Webformat\Http\SignedInteraction\Send\Director as SendDirector;

defined('B_PROLOG_INCLUDED') or exit('no prolog in "'.basename(__FILE__).'"!');

Loc::loadMessages(__FILE__);

class Form
{
    use Traits\Initable;

    protected static $moduleId = 'webformat.formtransmitter';

    public static function wfOnAfterResultAdd($formId, $resultId)
    {
        //Выходим, если это админка
        if (defined('ADMIN_SECTION') && \ADMIN_SECTION) {
            return true;
        }

        self::init();
        \Bitrix\Main\Loader::includeModule('form');

        if (!$memberIds = static::getConsumersSentTo($formId)) {
            return true;
        }

        $resultInfo = $t = [];
        $formFields = \CFormResult::getDataByID($resultId, [], $resultInfo, $t);
        $formData = \CForm::getByID($formId)->fetch();

        $request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

        $payload = [
            'cmd' => 'App\\Http\\SignedInteraction\\Cmd\\ReceiveFormData',
            'params' => [
                [
                    'type' => 'message',
                    'identity' => [ 'type' => 'webform', 'id' => $formId ],
                    'data' => [
                        'form' => $formData,
                        'fields' => $formFields,
                        'result' => $resultInfo,
                    ],
                    'extra' => [ 'trace' => $request->get('TRACE') ]
                ],
            ],
        ];
        $attachments = static::constructAttachments($formFields); //item: [path => '', title => '']

        foreach ($memberIds as $memberId) {
            $result = (new SendDirector(
                new BitrixSendKernel(['member_id' => $memberId])
            ))->send($payload, $attachments);
            // $result = $sender->send($payload, $attachments);
        }

        return true;

        /*
        $canonicalData = [];
        foreach($formData as $fieldCode => $values){
            $canonicalData[$fieldCode] = [];
            foreach($values as $value){
                if(!empty($value['USER_FILE_ID'])){
                    $canonicalData[$fieldCode][$value['USER_FILE_ID']] = $value['USER_TEXT'];
                }else{
                    $canonicalData[$fieldCode][] = $value['USER_TEXT'];
                }
            }
        }

        return true;
        */
    }

    /**
     * Метод возвращает список порталов (member_id), к которым подключена данная форма
     * (форма должна быть выбрана в настройках приложения соответствующего Битрикс24).
     */
    protected static function getConsumersSentTo($formId)
    {
        $storage = new ConsumerStorage();
        $memberIds = $storage->list();
        $membersToSent = [];
        foreach ($memberIds as $memberId) {
            $options = $storage->getOptions($memberId);
            if (!empty($options['forms']) &&
                \is_array($options['forms']) &&
                \in_array($formId, $options['forms'])
            ) {
                $membersToSent[] = $memberId;
            }
        }

        return $membersToSent;
    }

    protected static function constructAttachments(array $formFields): array
    {
        $attachments = [];
        if (empty($formFields)) {
            return $attachments;
        }

        foreach ($formFields as $fieldCode => $values) {
            foreach ($values as $value) {
                if (!empty($value['USER_FILE_ID']) && !isset($attachments[$value['USER_FILE_ID']])) {
                    $attachments[$value['USER_FILE_ID']] = [
                        'title' => $value['USER_TEXT'],
                        'path' => Loader::getDocumentRoot().\CFile::getPath($value['USER_FILE_ID']),
                    ];
                }
            }
        }

        return $attachments;
    }
}
