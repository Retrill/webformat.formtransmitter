<?php
namespace Webformat\FormTransmitter\Listeners\Traits;
use \Bitrix\Main\Localization\Loc;

defined('B_PROLOG_INCLUDED') or die('no prolog in "'.basename(__FILE__).'"!');

Loc::loadMessages(__FILE__);

trait Initable
{
    protected static $siteRoot;
	protected static $coreDir;
	protected static $moduleRoot;
	protected static $modulePath;

    protected static function init() {
		if (!isset(self::$siteRoot)) {
			self::$siteRoot = \Bitrix\Main\Loader::getDocumentRoot().'/';
		}
		if (!isset(self::$coreDir)) {
			self::$coreDir = \is_dir(self::$siteRoot.'local/modules/' . self::$moduleId) ? 'local' : 'bitrix';
		}
		if (!isset(self::$moduleRoot)) {
			self::$moduleRoot = self::$siteRoot.self::$coreDir.'/modules/' . self::$moduleId . '/';
		}
		if (!isset(self::$modulePath)) {
			self::$modulePath = '/' . self::$coreDir.'/modules/' . self::$moduleId . '/';
		}
	}
}
