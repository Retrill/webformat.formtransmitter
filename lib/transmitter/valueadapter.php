<?php

namespace Webformat\FormTransmitter\Transmitter;

use Bitrix\Main\Localization\Loc;

defined('B_PROLOG_INCLUDED') or exit('no prolog in "'.basename(__FILE__).'"!');

Loc::loadMessages(__FILE__);

class ValueAdapter
{
    protected $specification;
    protected $sources = [];
    protected $value = [];

    public function __construct()
    {
    }

    public function setSource(string $sourceCode, array $data)
    {
        $this->sources[$sourceCode] = $data;

        return $this;
    }

    public function setSpecification(array $specification)
    {
        $this->specification = $specification;

        return $this;
    }

    public function addSourceValue(string $template)
    {
        $regex = '/\{\{\s*(?<group>\S+)\.(?<param>\S+)\s*\}\}/';
        if (!(bool) \preg_match_all($regex, $template, $matches, \PREG_SET_ORDER)) {
            // this is a static value
            $this->value[] = \trim($template);

            return $this;
        }

        $textWithoutVars = \trim(\preg_replace($regex, '', $template));
        $flattenMode = (bool) $textWithoutVars;
        $flattenValue = \trim($template);

        foreach ($matches as $match) {
            $groupCode = $match['group'];
            $paramCode = $match['param'];
            $sourceValue = (array) ($this->sources[$groupCode][$paramCode] ?? []);

            if ($flattenMode) {
                $macro = $match[0]; // full match
                if (\is_array($sourceValue)) {
                    \sort($sourceValue);
                    $sourceValue = \implode(' / ', $sourceValue);
                }
                $flattenValue = \str_replace($macro, $sourceValue, $flattenValue);
            } else {
                if (\in_array($this->specification['type'], ['file', 'image'], true)) {
                    $this->value += $sourceValue; //fileId => fileTitle
                } else {
                    $this->value = \array_merge($this->value, $sourceValue);
                }
            }
        }

        if ($flattenMode) {
            $this->value[] = $flattenValue;
        }

        return $this;
    }

    public function getAdaptedValue()
    {
        $result = [];
        if ('crm_multifield' == $this->specification['type']) {
            foreach ((array) $this->value as $value) {
                $result[] = ['VALUE' => $value, 'VALUE_TYPE' => 'WORK'];
            }

            return $result;
        }
        if (\in_array($this->specification['type'], ['file', 'image'], true)) {
            foreach ((array) $this->value as $fileId => $fileTitle) {
                $result[] = [
                    'fileData' => [
                        $fileTitle,
                        \base64_encode(\file_get_contents(\Bitrix\Main\Loader::getDocumentRoot().\CFile::getPath($fileId))),
                    ],
                ];
            }

            return $this->specification['isMultiple'] ? $result : \current($result);
        }

        return $this->specification['isMultiple'] ? (array) $this->value : \implode(' / ', (array) $this->value);
    }
}
