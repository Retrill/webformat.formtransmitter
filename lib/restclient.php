<?php
namespace Webformat\FormTransmitter;
use \Bitrix\Main\Localization\Loc;

defined('B_PROLOG_INCLUDED') or die('no prolog in "'.basename(__FILE__).'"!');

Loc::loadMessages(__FILE__);

class RestClient
{
    protected $credentials = [];
    protected $credentialsDir = '';
    protected $endpoints = [];
    protected $http;

    public function __construct(){
        $this->credentialsDir = __DIR__.'/credentials/';
        $this->http = new \CHttp();
    }

    public function setCredentialsDir(string $path){
        $this->credentialsDir = \rtrim($path, '/').'/';
    }

    /**
     * @param string $url URL портала Б24 или же конкретного вебхука
     * @param string $memoCode Произвольный алиас для удобства использования (если не задан, то = URL)
     * @param string $clientId client id приложения (актуально только для случая, когда используется приложение)
     * @param string $clientSecret client secret приложения (актуально только для случая, когда используется приложение)
     */
    public function setEndpoint(string $url, string $memoCode = '', string $clientId = '', string $clientSecret = ''): void{
        if(!$memoCode){
            $memoCode = $url;
        }
        $memoCode = \trim($memoCode);
        $this->endpoints[$memoCode] = [
            'url' => \rtrim($url, '/').'/',
            'id' => \trim($clientId),
            'secret' => \trim($clientSecret),
        ];
        if($this->endpoints[$memoCode]['id']){
            $this->loadCredentials($memoCode);
        }
    }

    protected function loadCredentials(string $endpointMemo){
        $credentialsFile = $this->credentialsDir.$endpointMemo.'.php';
        if(!\file_exists($credentialsFile)){
            throw new \Exception('Credentials file does not exist!');
        }
        $this->credentials[$endpointMemo] = include $credentialsFile;
    }

    public function batch($queries)
    {
        $result = [];
        if (!\is_array($queries)) {
            return false;
        }
        while (\count($queries)) {
            $response = $this->exec('batch', [
                'CMD' => \array_splice($queries, 0, 50),
            ]);
            $result = \array_merge($result, $response['result']['result']);
            if (!$response['result']['result']) {
                return false;
            }
        }

        return $result;
    }

    /* protected function batchRequest($data, $method)
    {
        $nPageSize = 0;
        $iNumPage = 50;
        $arBatch = [];
        $queryData = \http_build_query($data);
        \parse_str(\str_replace(['__--SiZe--__', '__--PaGe--__', '__--StArT--__'], 1, $queryData), $queryData1);
        //$result = getBitrixApi($queryData1, $method); //var_dump($queryData1,$result); die();
        $result = $this->exec($method, $queryData1, $_REQUEST['auth']);
        if (!isset($result['total'],$result['result'])) {
            return false;
        }
        if ($result['total'] < 2) {
            return [$result['result']];
        }
        $nPageSize = (($result['total'] % $iNumPage) > 0 ? (intval($result['total'] / $iNumPage) + 1) : ($result['total'] / $iNumPage));
        for ($x = 0; $x++ < $nPageSize;) {
            $arBatch[] = $method.'?'.str_replace(['__--SiZe--__', '__--PaGe--__', '__--StArT--__'], [$iNumPage, $x, ($x - 1) * $iNumPage], $queryData);
        }
        $result = [];
        while (count($arBatch)) {
            //$res = getBitrixApi(['CMD'=>array_splice($arBatch,0,50)],'batch');
            $res = restCommand('batch', ['CMD' => array_splice($arBatch, 0, 50)], $_REQUEST['auth']);
            $result = array_merge($result, $res['result']['result']);
        }
        if ($nPageSize != count($result)) {
            return false;
        }

        return $result;
    } */

    public function exec(string $method, array $params = [], ?string $endpointOnly = null)
    {
        $return = [];
        foreach($this->endpoints as $memo => $endpoint){
            if(!is_null($endpointOnly) && ($memo != $endpointOnly)){
                continue;
            }
            $queryUrl = $endpoint['url'].$method.'.json';
            if(!empty($endpoint['id'])){
                $this->actualizeCredentials($memo);
                $params = ['auth' => $this->credentials[$memo]['access_token']] + $params;
            }

            $response = $this->http->post($queryUrl, $params);
            $response = \json_decode($response, true);

            if (isset($response['error'])
                && \in_array($response['error'], ['expired_token', 'invalid_token'])
                && $this->refreshCredentials($memo)
            ) {
                $response = $this->{__FUNCTION__}($method, $params, $memo)[$memo];
            }

            $return[$memo] = $response;
        }

        return $return;
    }

    public function saveCredentials(string $endpointMemo, array $credentials, $onlyIfChanged = true)
    {
        $credentialsFile = $this->credentialsDir.$endpointMemo.'.php';

        $fileExists = \file_exists($credentialsFile);
        if (!$onlyIfChanged || !$fileExists) {
            $storedCredentials = ['access_token' => ''];
        } else {
            $storedCredentials = include $credentialsFile;
        }

        if ($storedCredentials['access_token'] == $credentials['access_token']) {
            return false;
        }

        if (!\file_put_contents(
                $credentialsFile,
                "<?php\nreturn ".\var_export($credentials, true).";\n"
        )) {
            throw new \Exception('Can\'t save credentials file!');
        }

        return true;
    }

    protected function refreshCredentials(string $endpointMemo)
    {
        if (!isset($this->credentials[$endpointMemo]['refresh_token']) || !isset($this->endpoints[$endpointMemo])) {
            return false;
        }
        $endpoint = $this->endpoints[$endpointMemo];

        $queryUrl = 'https://oauth.bitrix.info/oauth/token/';
        $queryParams = [
            'grant_type' => 'refresh_token',
            'client_id' => $endpoint['id'],
            'client_secret' => $endpoint['secret'],
            'refresh_token' => $this->credentials[$endpointMemo]['refresh_token'],
        ];
        $response = $this->http->post($queryUrl, $queryParams);
        $response = \json_decode($response, true);

        if (!\is_array($response) || isset($response['error'])) {
            return false;
        }

        $response['expires'] = \time() + (int) $response['expires_in'] - 5;
        $this->credentials[$endpointMemo] = $response;
        $this->saveCredentials($endpointMemo, $response);

        return $response;
    }

    protected function actualizeCredentials(string $endpointMemo): void
    {
        if (\time() > $this->credentials[$endpointMemo]['expires']) {
            $this->refreshCredentials($endpointMemo);
        }

        return;
    }
}
