<?php

namespace Webformat\FormTransmitter;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Options
{
    public const LANG_PREFIX = 'WEBFORMAT_FORMTRANSMITTER_OPTIONS_';

    public static function save(array $options, string $filename = 'options', $subdir = 'inc')
    {
        // $filename = \ltrim($filename ?: 'options', './');

        // remove leading "./"
        $filename = \preg_replace('#^\.\/#', '', $filename ?: 'options');
        $options = static::trimRecursive($options);
        /*
        foreach($options as $key => $item){
            if(!is_array($item)){continue;}
            $options[$key] = array_map('trim', array_filter($item));
        }
        */
        $subdir = \str_replace('../', '', \trim($subdir, '/'));
        $phpContent = '<?php'."\n".'return '.\var_export($options, true).';'."\n";

        if ('/' === \mb_substr($filename, 0, 1, 'UTF-8')) {
            // absolute path notation
            $targetFile = $filename;
        } else {
            // relative path notation
            $targetFile = \dirname(__DIR__)."/$subdir/$filename.php";
        }

        $putResults = \file_put_contents($targetFile, $phpContent);
        if (!(bool) $putResults) {
            /* echo \CAdminMessage::ShowMessage([
                'TYPE' => 'ERROR',
                'MESSAGE' => Loc::getMessage(self::LANG_PREFIX.'SAVE_FAILURE')
            ]); */
            throw new \Exception(Loc::getMessage(self::LANG_PREFIX.'SAVE_FAILURE'));
        }

        return true;
    }

    protected static function trimRecursive($m)
    {
        if (is_scalar($m)) {
            return trim($m);
        }
        if (is_array($m)) {
            foreach ($m as $index => $child) {
                $m[$index] = static::{__FUNCTION__}($child);
            }
        }

        return $m;
    }

    public static function get($name = 'options', $allowEmpty = false)
    {
        $name = \ltrim($name, './');
        $optionsDir = \dirname(__DIR__).'/inc/';
        $targetFile = $optionsDir.$name.'.php';
        if (!\file_exists($targetFile)) {
            $targetFile = $optionsDir.$name.'.default.php';
        }

        if (\file_exists($targetFile)) {
            $options = include $targetFile;
        } elseif (!$allowEmpty) {
            \ShowError(Loc::getMessage(self::LANG_PREFIX.'NONEXISTENT_DEFAULT_FILE'));
        } else {
            $options = [];
        }

        return $options;
    }

    public static function reset()
    {
        $targetFile = dirname(__DIR__).'/inc/options.php';
        if (file_exists($targetFile) && !unlink($targetFile)) {
            \ShowError(Loc::getMessage(self::LANG_PREFIX.'UNLINK_FAILURE'));

            return false;
        }

        return true;
    }

    public static function getStorageDir()
    {
        $options = static::get();
        $storageDir = $options['storage_dir'] ?: '#module_root#/storage';
        $storageDir = \strtr($storageDir, [
            '#module_root#' => \rtrim(\dirname(__DIR__), '/'),
            '#document_root#' => \rtrim($_SERVER['DOCUMENT_ROOT'], '/'),
        ]);
        if (!\is_dir($storageDir) &&
            !\mkdir($storageDir, \defined('BX_DIR_PERMISSIONS') ? \BX_DIR_PERMISSIONS : 0755, true)
        ) {
            return null;
        }

        return $storageDir;
    }
}
