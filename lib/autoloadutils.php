<?php

namespace Webformat\FormTransmitter;

class AutoloadUtils
{
    public static function getBasedir()
    {
        static $basedir;
        if (isset($basedir)) {
            return $basedir;
        }
        $basedir = \rtrim(\dirname(__DIR__), '/').'/';

        return $basedir;
    }

    public static function enableAutoload()
    {
        \spl_autoload_register([self, 'autoLoad'], true, true);
    }

    protected static function autoLoad($fullyQualifiedClassName)
    {
        $psr4 = [
            'Webformat\\Http\\SignedInteraction\\' => 'lib/http/signedinteraction',
        ];

        $subdir = null;
        $filepath = null;
        foreach ($psr4 as $leadingNamespace => $subdir) {
            if (0 !== \mb_strpos($fullyQualifiedClassName, $leadingNamespace, 0)) {
                continue;
            }
            $filepath = \mb_substr($fullyQualifiedClassName, \mb_strlen($leadingNamespace));
            $filepath = self::getBasedir().$subdir.'/'.\str_replace('\\', \DIRECTORY_SEPARATOR, $filepath).'.php';
        }

        if (empty($subdir) || empty($filepath) || !\file_exists($filepath)) {
            return false;
        }
        include $filepath;

        return true;
    }
}
