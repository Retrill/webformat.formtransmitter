<?php

namespace Webformat\FormTransmitter;

use Bitrix\Main\Localization\Loc;
use Webformat\FormTransmitter\Http\BitrixSendKernel;
use Webformat\Http\SignedInteraction\Send\Director as SendDirector;

defined('B_PROLOG_INCLUDED') or exit('no prolog in '.\basename(__FILE__).'!');

Loc::loadMessages(__FILE__);

class Connector
{
    protected $langPrefix = 'WEBFORMAT_FORMTRANSMITTER_CONNECTOR_';
    protected $errors = [];
    protected $handshakeEndpoint;
    protected $consumerStorage;

    public function __construct()
    {
        $options = Options::get();
        $this->consumerStorage = new ConsumerStorage();
        $this->handshakeEndpoint = \rtrim($options['app_host'], '/').'/'.\ltrim($options['handshake_endpoint'], '/');
    }

    public function handshake(array $request)
    {
        $this->errors = [];
        if (!$credentials = $this->canonicalize($request)) {
            $this->errors[] = Loc::getMessage($this->langPrefix.'NONCANONICAL_CREDENTIALS');

            return false;
        }
        $hostId = (int) ($request['host_id'] ?: 0);
        if ($hostId <= 0) {
            $this->errors[] = Loc::getMessage($this->langPrefix.'EMPTY_HOST_ID');

            return false;
        }

        $keyPair = $this->tryGenerateKey();
        $publicKeyPem = '';
        if (\is_resource($keyPair)) {
            try {
                $this->consumerStorage->saveKey2Enc($keyPair, $credentials['member_id']);
            } catch (\Throwable $er) {
                $this->errors[] = Loc::getMessage($this->langPrefix.'PKEY_SAVE_ERROR')."\n".$er->getMessage();

                return false;
            }
            $publicKeyPem = \openssl_pkey_get_details($keyPair)['key'];
        }

        $b24options = [
            'my_host_id' => $hostId,
            'server_name' => $credentials['DOMAIN'],
            'member_id' => $credentials['member_id'],
            // 'remote_public_key' => $publicKeyPem,
        ];
        if (!$this->consumerStorage->saveOptions($b24options, $credentials['member_id'])) {
            $this->errors[] = Loc::getMessage($this->langPrefix.'CONSUMER_OPTIONS_SAVE_ERROR');

            return false;
        }

        $requestParams = $credentials + [
            'key2dec' => $publicKeyPem,
            '_method' => 'patch',
        ];

        $endpoint = \str_replace('#host_id#', (string) $hostId, $this->handshakeEndpoint);
        if (!$remotePayload = $this->getPayloadByRequest($endpoint, $requestParams)) {
            return false;
        }

        $b24options['salt'] = $remotePayload['salt'];
        if (!$this->consumerStorage->saveOptions($b24options, $credentials['member_id'])) {
            $this->errors[] = Loc::getMessage($this->langPrefix.'CONSUMER_OPTIONS_SAVE_ERROR');

            return false;
        }

        try {
            $this->consumerStorage->saveKey2Dec(\base64_decode($remotePayload['key2dec']), $credentials['member_id']);
        } catch (\Throwable $er) {
            $this->errors[] = Loc::getMessage($this->langPrefix.'PUBLIC_KEY_SAVE_ERROR')."\n".$er->getMessage();

            return false;
        }

        return true;
    }

    protected function canonicalize(array $credentials): array
    {
        $required = \array_flip(['AUTH_ID', 'AUTH_EXPIRES', 'REFRESH_ID', 'member_id', 'DOMAIN', 'LANG', 'APP_SID']);
        foreach ($required as $fieldName => $value) {
            if (empty($credentials[$fieldName]) || !(bool) \trim($credentials[$fieldName])) {
                return [];
            }
        }

        return \array_map('trim', \array_intersect_key($credentials, $required));
    }

    protected function tryGenerateKey()
    {
        if (!\extension_loaded('openssl') || !\function_exists('openssl_pkey_new')) {
            return null;
        }

        $config = [
            'digest_alg' => 'sha512',
            'private_key_bits' => 4096,
            'private_key_type' => \OPENSSL_KEYTYPE_RSA,
        ];
        if (!$keyRes = \openssl_pkey_new($config)) {
            return null;
        }

        return $keyRes;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    protected function getStorageDir(string $memberId): string
    {
        $storageDir = \dirname(__DIR__).'/storage';
        $dir = $storageDir.'/'.$memberId;
        if (!\is_dir($dir)) {
            \mkdir($dir, \defined('BX_DIR_PERMISSIONS') ? \BX_DIR_PERMISSIONS : 0755, true);
        }
        if (!file_exists($storageDir.'/.htaccess')) {
            \file_put_contents($storageDir.'/.htaccess', "deny from all\n");
        }

        return $dir;
    }

    protected function post(string $url, array $requestData, &$responseCode)
    {
        // $header = 'Authorization: Basic '.\base64_encode('u:p')."\r\n";
        $header .= "Content-type: application/x-www-form-urlencoded\r\n";
        $header .= "User-Agent: Bitrix Site Manager handshaker\r\n";
        // $header .= "Accept: */*\r\n";
        $header .= "Cache-Control: no-cache, no-store, no-transform\r\n";
        $header .= "Accept: application/json\r\n";

        $header .= "Connection: close\r\n";

        $context = \stream_context_create([
            'http' => [
                'method' => 'POST',
                'ignore_errors' => true,
                'header' => $header,
                'content' => \http_build_query($requestData),
            ],
            // 'ssl' => ['verify_peer' => false, 'verify_peer_name' => false]
        ]);
        $response = \file_get_contents($url, false, $context);
        $responseCode = $this->getResponseCode($http_response_header);

        return $response;
    }

    protected function getResponseCode(?array $responseHeaders): int
    {
        if (!\is_null($responseHeaders) && (bool) \preg_match('#HTTP/[\d\.]+\s+(?<code>\d+)#', $responseHeaders[0], $matches)) {
            return (int) $matches['code'];
        }

        return 0;
    }

    protected function getPayloadByRequest(string $endpoint, array $requestParams): ?array
    {
        $httpResponseCode = 0;
        $response = $this->post($endpoint, $requestParams, $httpResponseCode);

        if (403 == $httpResponseCode) {
            $this->errors[] = Loc::getMessage($this->langPrefix.'AUTHENTICATION_ERROR');

            return null;
        }

        $response = \json_decode($response, true);
        if (empty($response)) {
            $this->errors[] = Loc::getMessage($this->langPrefix.'EMPTY_REPSONSE');

            return null;
        }

        if (!empty($response['errors'])) {
            $this->errors = array_merge($this->errors, $response['errors']);
        }

        if (empty($response['data']) || !is_array($response['data'])) {
            $this->errors[] = Loc::getMessage($this->langPrefix.'EMPTY_REPSONSE');

            return null;
        }

        return $response['data'];
    }

    public function sayHello(string $memberId): array
    {
        $sender = new SendDirector(new BitrixSendKernel(['member_id' => $memberId]));

        $filepaths = \glob(\dirname(__DIR__).'/assets/example-files/*');
        $attachments = [];
        $payload = [
            'cmd' => 'App\Http\SignedInteraction\Cmd\Hello',
            'params' => [
                'text' => 'Hello! Это я!',
            ],
        ];

        $hashExpected = [];
        foreach ($filepaths as $filepath) {
            $attachments[] = [
                'path' => $filepath,
                'title' => \basename($filepath),
                // 'hash' => \md5_file($filepath)
            ];
            $hashExpected[] = \md5_file($filepath);
        }

        try {
            $remoteResults = $sender->send($payload, $attachments);
        } catch (\Throwable $er) {
            return [
                'status' => 'error',
                'errors' => [$er->getMessage()],
            ];
        }
        if (!$remoteResults = \json_decode($remoteResults, true)) {
            return [
                'status' => 'error',
                'errors' => ['The remote host returns a malformed JSON'],
            ];
        }
        if (
            ('ok' != $remoteResults['status']) ||
            empty($remoteResults['data']['hello']) ||
            ($hashExpected != ($remoteResults['data']['attachments'] ?: []))
        ) {
            return [
                'status' => 'error',
                'errors' => \array_merge(['A remote app host returns a malformed or incorrect JSON data'], $remoteResults['errors'] ?: []),
            ];
        }

        return [
            'status' => 'ok',
            'errors' => [],
        ];
    }
}
