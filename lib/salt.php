<?php
namespace Webformat\FormTransmitter;
use \Bitrix\Main\Localization\Loc;

defined('B_PROLOG_INCLUDED') or die('no prolog in ''.basename(__FILE__).''!');

Loc::loadMessages(__FILE__);

class Salt{
    public static function random(int $length = 20, string $dict = 'A-Z a-z \d', string $boundarySafeDict = '')
    {
        if (!strlen($dict)) {
            throw new \Exception('Dictionary cannot be empty!');
        }
        $boundarySafeDict = $boundarySafeDict ?: $dict;
        $arDict = \preg_split('/\s+/', $dict);
        $arBoundarySafeDict = \preg_split('/\s+/', $boundarySafeDict);
        $arDict = \array_unique([...$arDict, ...$arBoundarySafeDict]);

        return static::strGenerateCanonical($length, $arDict, $arBoundarySafeDict);
    }

    protected static function strGenerateCanonical(int $length, array $dict, array $boundarySafeDict)
    {
        if (\count($dict) > $length) {
            throw new \Exception('Expected string is too short to be consisted of all symbol classes!');
        }
        $word = [];

        // count of word symbols can't be less than count of symbol classes
        foreach ($dict as $class) {
            $word[] = static::chooseSymbolFromClass($class);
        }

        $remainingCnt = $length - count($word); // may be <0

        $useTipControl = $remainingCnt >= 2; //Если меньше 2 - мы не сможем использовать символ каждого класса
        if ($useTipControl) {
            $remainingCnt -= 2;
        }
        for ($r = 1; $r <= $remainingCnt; ++$r) {
            $word[] = static::chooseSymbolFromDict($dict);
        }
        shuffle($word);

        if ($useTipControl) {
            array_unshift($word, static::chooseSymbolFromDict($boundarySafeDict));
            $word[] = static::chooseSymbolFromDict($boundarySafeDict);
        }

        return implode('', $word);
    }

    protected static function chooseSymbolFromDict(array $dictionary)
    {
        $class = implode('', $dictionary);

        return static::chooseSymbolFromClass($class);
    }

    protected static function chooseSymbolFromClass(string $class)
    {
        static $haystack;
        if (!isset($haystack)) {
            // utf-codes, see https://www.w3schools.com/charsets/ref_utf_basic_latin.asp
            $symbols = [
                ...range(32, 126), // ascii printable symbols, see https://theasciicode.com.ar/
                ...range(1040, 1103), // cyrillic alphabet uppercase && lowercase
                1025, // cyrillic IO uppercase
                1105, // cyrillic IO lowercase
            ];
            $symbols = \array_map('mb_chr', $symbols, \array_fill(0, \count($symbols), 'UTF-8'));
            $haystack = \implode('', $symbols);
        }

        \preg_match_all('/['.$class.']/', $haystack, $matches);
        if (empty($matches[0])) {
            throw new \Exception('No symbol found by class definition ['.$class.']!');
        }
        $index = \mt_rand(0, \count($matches[0]) - 1);
        return $matches[0][$index];
    }
}