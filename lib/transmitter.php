<?php
namespace Webformat\FormTransmitter;
use \Bitrix\Main\Localization\Loc;

defined('B_PROLOG_INCLUDED') or die('no prolog in "'.basename(__FILE__).'"!');

Loc::loadMessages(__FILE__);

class Transmitter
{
    protected $client;
    protected $options;

    public function __construct()
    {
        $this->options = Options::get();

        $this->client = new RestClient();
        $this->client->setCredentialsDir(\dirname(__DIR__).'/credentials');
        foreach($this->options['endpoints'] as $memo => $endpoint){
            $this->client->setEndpoint($endpoint['url'], $memo, $endpoint['id'], $endpoint['secret']);
        }
    }

    public function send(int $formId, array $data): array{
        \Bitrix\Main\Loader::includeModule('form');
        $sendResult = [];
        $entityName = $this->getEntityName($formId);
        foreach($this->options['endpoints'] as $endpointMemo => $endpoint){
            $dataAdapted = (empty($endpoint['id']) || empty($this->options['matches'][$formId])) ? $data : $this->adaptFormData($data, $this->options['matches'][$formId], $endpointMemo, $entityName);
            $dataAdapted = $this->doReplacements($dataAdapted, [
                'form' => \array_change_key_case(\CForm::getByID($formId)->fetch(), \CASE_LOWER),
                'result' => \array_change_key_case($data)
            ]);

            $args = [
                'fields' => $dataAdapted,
                'params' => ['REGISTER_SONET_EVENT' => 'N']
            ];

            $methodName = $entityName.'.add';
            if((bool)\preg_match('#^crm\.item\.(?<type_id>\d+)$#', $entityName, $matches)){
                $methodName = 'crm.item.add';
                $args['entityTypeId'] = $matches['type_id'];
                unset($args['params']);
            }
            $sendResult[$endpointMemo] = $this->client->exec($methodName, $args, $endpointMemo)[$endpointMemo];
        }

        return $sendResult;
    }

    protected function doReplacements($values, $replacements){
        if(\is_scalar($values)){
            return \preg_replace_callback('/\{\{\s*(?<group>\S+)\.(?<param>\S+)\s*\}\}/', function($matches) use ($replacements){
                $newValue = $replacements[$matches['group']][$matches['param']];
                if(\is_array($newValue)){
                    $newValue = \implode(' / ', $newValue);
                }
                return $newValue;
            }, $values);
        }elseif(\is_array($values)){
            foreach($values as $index => $value){
                $values[$index] = $this->{__FUNCTION__}($value, $replacements);
            }
        }

        return $values;
    }

    protected function adaptFormData(array $sourceData, array $matches, string $endpointMemo, string $entityName){
        $adapted = [];

        $args = [];
        if ((bool) \preg_match('#^crm\.item\.(?<type_id>\d+)$#', $entityName, $itemData)) {
            $entityName = 'crm.item';
            $args['entityTypeId'] = $itemData['type_id'];
        }

        $targetFields = $this->client->exec($entityName.'.fields', $args, $endpointMemo)[$endpointMemo]['result'];
        if(isset($targetFields['fields'])){
            // smart project case
            $targetFields = $targetFields['fields'];
        }
        $allCodes = \array_keys($targetFields);
        $allCodeIndex = \array_combine(
            \array_map(function($v) { return \str_replace('_', '', \mb_strtolower($v)); }, $allCodes),
            $allCodes
        );

        foreach($matches as $b24Code => $localCode){
            if(\mb_substr($localCode, 0, 1, 'UTF-8') == '='){
                $value = \mb_substr($localCode, 1, null, 'UTF-8');
            }else{
                $value = $sourceData[$localCode] ?? '';
            }
            if($canonicalCode = $this->getCanonicalB24Code($b24Code, $allCodeIndex)){
                // the field exists at Bitrix24
                $value = $this->adaptValue($value, $targetFields[$canonicalCode], $entityName);
            }
            // we should save "TRACE" field (it isn't a canonical code)
            $adapted[$canonicalCode ?: $b24Code] = $value;
        }

        return $adapted;
    }

    protected function getCanonicalB24Code(string $code, array $allCodeIndex): ?string{
        $code = \str_replace('_', '', \mb_strtolower($code));
        return $allCodeIndex[$code] ?? null;
    }

    protected function adaptValue($value, $specification, string $entityName){
        $specification = \array_change_key_case($specification, \CASE_LOWER);
        if(empty($value)){
            return $specification['ismultiple'] ? [] : (string)$value;
        }
        if($specification['ismultiple']){
            if(\is_scalar($value)){
                $value = [$value];
            }
            if($specification['type'] == 'crm_multifield'){
                foreach($value as $index => $singleValue){
                    $value[$index] = ['VALUE' => $singleValue, 'VALUE_TYPE' => 'WORK'];
                }
            }
            if($specification['type'] == 'file'){
                foreach($value as $fileId => $fileName){
                    $finfo = [
                        $fileName,
                        \base64_encode(\file_get_contents(\Bitrix\Main\Loader::getDocumentRoot().\CFile::getPath($fileId)))
                    ];
                    $value[$fileId] = ('crm.item' == $entityName) ? $finfo : ['fileData' => $finfo];
                }
                $value = \array_values($value);
            }
        }else{
            if($specification['type'] == 'file'){
                $fileId = \current(\array_keys($value));
                $finfo = [
                    $value[$fileId],
                    \base64_encode(\file_get_contents(\Bitrix\Main\Loader::getDocumentRoot().\CFile::getPath($fileId)))
                ];
                return ('crm.item' == $entityName) ? $finfo : ['fileData' => $finfo];
                /*
                return [
                    'fileData' => [
                        $value[$fileId],
                        \base64_encode(\file_get_contents(\Bitrix\Main\Loader::getDocumentRoot().\CFile::getPath($fileId)))
                    ]
                ];
                */
            }
            if(\is_array($value)){
                $value = \implode(' / ', $value);
            }
            if($specification['type'] == 'crm_multifield'){
                $value = ['VALUE' => $value, 'VALUE_TYPE' => 'WORK'];
            }
        }


        return $value;
    }

    protected function getEntityName(int $formId): string{
        if(empty($this->options['entities']) || !is_array($this->options['entities'])){
            return 'crm.lead';
        }
        foreach($this->options['entities'] as $entityName => $formIds){
            if(\in_array($formId, $formIds, true)){

                return $entityName;
            }
        }
        return 'crm.lead';
    }
}
