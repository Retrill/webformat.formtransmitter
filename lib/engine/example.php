<?php
namespace Webformat\FormTransmitter\Engine;
use \Bitrix\Main\Localization\Loc;
use \Webformat\FormTransmitter\Options;

defined('B_PROLOG_INCLUDED') or die('no prolog in "'.basename(__FILE__).'"!');
\Bitrix\Main\Loader::includeModule('webformat.loadapi');

Loc::loadMessages(__FILE__);

class Example extends \Webformat\LoadApi\Engine{
    protected $moduleRoot;
    protected $ticket;

	protected function init(){
        $this->moduleRoot = \Webformat\FormTransmitter\CliUtils::getBasedir();
	}
	
	protected function getCounterDefinition(){
        return array(
            //'param' => 'title',
        );
    }
    
    protected function execute($arParams){
    	
	}
	
	protected function initLogger() {
        $this->log = new \Webformat\LoadApi\Logger\NullObject('/dev/null');
    }
}
