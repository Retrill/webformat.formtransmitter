<?php

namespace Webformat\FormTransmitter;

use Bitrix\Main\Localization\Loc;

defined('B_PROLOG_INCLUDED') or exit('no prolog in "'.basename(__FILE__).'"!');

Loc::loadMessages(__FILE__);

class ConsumerStorage
{
    protected $dir;

    public function saveOptions($options, $memberId)
    {
        if (!$consumerDir = $this->getConsumerDir($memberId)) {
            return false;
        }

        return Options::save($options, $consumerDir.'/options.php');
    }

    public function saveKey2Enc($keyPair, $memberId){
        if(!\is_resource($keyPair) || empty($memberId)){
            throw new \Exception('Empty key pair or member id!');
        }
        if (!$consumerDir = $this->getConsumerDir($memberId)) {
            throw new \Exception('Can\'t specify consumer directory path!');
        }
        $key2EncPath = $consumerDir.'/key2enc.php';
        if(\file_exists($key2EncPath) && (\filesize($key2EncPath) > 0)){
            throw new \Exception('Private key already exists!');
        }
        if (!\openssl_pkey_export_to_file($keyPair, $key2EncPath)) {
            throw new \Exception('Can\'t write private key data!');
        }

        return true;
    }

    public function saveKey2Dec($keyString, $memberId){
        if(empty($keyString) || empty($memberId)){
            throw new \Exception('Empty public key or member id!');
        }
        if (!$consumerDir = $this->getConsumerDir($memberId)) {
            throw new \Exception('Can\'t specify consumer directory path!');
        }
        $keyPath = $consumerDir.'/key2dec.php';
        if(\file_exists($keyPath) && (\filesize($keyPath) > 0)){
            throw new \Exception('Public key already exists!');
        }
        if (!\file_put_contents($keyPath, $keyString)) {
            throw new \Exception('Can\'t write public key data!');
        }

        return true;
    }

    public function getOptions($memberId)
    {
        if (!$consumerDir = $this->getConsumerDir($memberId)) {
            return false;
        }
        $optionsFile = $consumerDir.'/options.php';
        if (!file_exists($optionsFile)) {
            return [];
        }
        $options = include $optionsFile;

        return $options;
    }

    protected function getDir()
    {
        if (isset($this->dir)) {
            return $this->dir;
        }
        $options = Options::get();
        $storageDir = $options['storage_dir'] ?: '#module_root#/storage';
        $storageDir = \strtr($storageDir, [
            '#module_root#' => \rtrim(\dirname(__DIR__), '/'),
            '#document_root#' => \rtrim($_SERVER['DOCUMENT_ROOT'], '/'),
        ]);
        if (!\is_dir($storageDir) &&
            !\mkdir($storageDir, \defined('BX_DIR_PERMISSIONS') ? \BX_DIR_PERMISSIONS : 0755, true)
        ) {
            return null;
        }

        if (!file_exists($storageDir.'/.htaccess') &&
            !\file_put_contents($storageDir.'/.htaccess', "deny from all\n")
        ) {
            return null;
        }

        $this->dir = $storageDir;

        return $storageDir;
    }

    protected function getConsumerDir($memberId)
    {
        $consumerDir = $this->getDir()."/$memberId";
        if (!\is_dir($consumerDir) &&
            !\mkdir($consumerDir, \defined('BX_DIR_PERMISSIONS') ? \BX_DIR_PERMISSIONS : 0755, true)
        ) {
            return null;
        }

        return $consumerDir;
    }

    public function list()
    {
        if (!$dirs = \glob($this->getDir().'/*', \GLOB_ONLYDIR)) {
            return [];
        }
        $dirs = \array_map('basename', $dirs);

        return $dirs;
    }

    public function getKey2Enc($memberId){
        if(empty($memberId)){
            return null;
        }
        $pathToKey = \rtrim($this->getConsumerDir($memberId), '/').'/key2enc.php';
        if(!\file_exists($pathToKey)){
            return null;
        }
        $keyContents = \file_get_contents($pathToKey);

        return $keyContents ?: null;
    }

    public function getKey2Dec($memberId){
        if(empty($memberId)){
            return null;
        }
        $pathToKey = \rtrim($this->getConsumerDir($memberId), '/').'/key2dec.php';
        if(!\file_exists($pathToKey)){
            return null;
        }

        return \file_get_contents($pathToKey) ?: null;
    }

    public function remove($memberId)
    {
        $memberId = \preg_replace('/[^\d\w]+/', '', $memberId);
        if(!$memberId){
            return false;
        }

        $consumerDir = $this->getDir()."/$memberId";
        if(!\is_dir($consumerDir)){
            return true;
        }

        return $this->removeDirRecursive($consumerDir);
    }

    protected function removeDirRecursive($path)
    {
        $subitems = \preg_grep('/\/\.+$/', \glob($path.'/{,.}*', \GLOB_BRACE), \PREG_GREP_INVERT);
        foreach ($subitems as $subitem) {
            /* if(\is_dir($subitem)){
                $this->{__FUNCTION__}($subitem);
            }else{
                echo $subitem."\n";
            } */
            $removeResults = \is_dir($subitem) ? $this->{__FUNCTION__}($subitem) : \unlink($subitem);
            if(!$removeResults){
                return false;
            }
        }
        return \rmdir($path);
    }
}
