<?php

namespace Webformat\FormTransmitter\Http\Cmd;

use Webformat\Http\SignedInteraction\Receive\Cmd\BaseCommand;

defined('B_PROLOG_INCLUDED') or exit('no prolog!');

class WebformIndex extends BaseCommand
{
    public function __invoke(...$params): array
    {
        if(!\Bitrix\Main\Loader::includeModule('form')){
            throw new \Exception('No "form" module installed!');
        }

        $res = \CForm::getList($by = 'id', $order = 'asc', [], $isFiltered = null);
        $forms = [];
        while($form = $res->fetch()){
            $forms[] = ['id' => (int)$form['ID'], 'name' => \trim($form['NAME'])];
        }
        return $forms;
    }
}
