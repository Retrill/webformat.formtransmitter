<?php

namespace Webformat\FormTransmitter\Http\Cmd;

use Webformat\Http\SignedInteraction\Receive\Cmd\BaseCommand;
use Webformat\FormTransmitter\ConsumerStorage;

defined('B_PROLOG_INCLUDED') or exit('no prolog!');

class Disconnect extends BaseCommand
{
    public function __invoke(...$params): array
    {
        if(!$memberId = ($params[0] ?: '')){
            throw new \Exception('Empty member id');
        }
        if(!$hostId = ($params[1] ?: '')){
            throw new \Exception('Empty host id');
        }
        $hostId = (int) $hostId;
        $consumerStorage = new ConsumerStorage();

        if(!\in_array((string)$memberId, $consumerStorage->list(), true)){
            return ['nothing to remove'];
        }

        $consumerOptions = $consumerStorage->getOptions($memberId);
        if(empty($consumerOptions['my_host_id']) || ($consumerOptions['my_host_id'] != $hostId)){
            throw new \Exception('Wrong host id');
        }

        \ob_start();
        echo "member id: $memberId\n";
        if(!$consumerStorage->remove($memberId)){
            throw new \Exception('Removing error');
        }
        $dump = explode("\n", \ob_get_contents());
        \ob_end_clean();

        return ['removed succefully', $memberId, $dump];
    }
}
