<?php

namespace Webformat\FormTransmitter\Http\Cmd;

use Webformat\Http\SignedInteraction\Receive\Cmd\BaseCommand;
use Webformat\FormTransmitter\ConsumerStorage;

defined('B_PROLOG_INCLUDED') or exit('no prolog!');

class SaveFormIds extends BaseCommand
{
    public function __invoke(...$params): array
    {
        if(!$memberId = ($params[0] ?: '')){
            throw new \Exception('Empty member id');
        };
        $formIds = $params[1] ?: [];
        $consumerStorage = new ConsumerStorage();

        if(!$options = $consumerStorage->getOptions($memberId)){
            throw new \Exception('Empty consumer storage options');
        }

        $options['forms'] = $formIds;

        if (!$consumerStorage->saveOptions($options, $memberId)) {
            throw new \Exception('Consumer options saving error!');
        }
        return [\count($formIds).' ids saved'];
    }
}
