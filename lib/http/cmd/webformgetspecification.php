<?php

namespace Webformat\FormTransmitter\Http\Cmd;

use Webformat\Http\SignedInteraction\Receive\Cmd\BaseCommand;

defined('B_PROLOG_INCLUDED') or exit('no prolog!');

class WebformGetSpecification extends BaseCommand
{
    public function __invoke(...$params): array
    {
        if(!\Bitrix\Main\Loader::includeModule('form')){
            throw new \Exception('No "form" module installed!');
        }

        $specification = [];
        $res = \CForm::getList($by = 'id', $order = 'asc', [], $isFiltered = null);
        while($form = $res->fetch()){
            $specification[$form['ID']] = [];
            $questionRes = \CFormField::getList(
                $form['ID'], 'N', $by = 's_id', $order='desc',
                ['ACTIVE' => 'Y'], $isFiltered
            );
            while($question = $questionRes->fetch()){
                $answer = \CFormAnswer::getList(
                    $question['ID'], $by = 's_sort', $order='asc', [], $isFiltered
                )->fetch();
                $fieldCode = \mb_strtolower($question['SID'], 'UTF-8');
                $specification[$form['ID']][$fieldCode] = [
                    'type' => $answer['FIELD_TYPE'],
                    'title' => $question['TITLE'],
                    'code' => $fieldCode
                ];
            }
        }

        return $specification;
    }
}
