<?php

namespace Webformat\FormTransmitter\Http;

use Webformat\FormTransmitter\ConsumerStorage;
use Webformat\FormTransmitter\Options;
use Webformat\Http\SignedInteraction\Receive\Kernel;
use Bitrix\Main\HttpRequest as Request;

defined('B_PROLOG_INCLUDED') or exit('no prolog in '.\basename(__FILE__).'!');

class BitrixReceiveKernel extends Kernel
{
    protected $memberId;
    protected $attachments = [];

    public function requestIsValid(): bool
    {
        if(!parent::{__FUNCTION__}()){
            return false;
        }

        if(!($this->request instanceof Request) && !\is_subclass_of($this->request, Request::class))
        {
            $this->response['status'] = 'error';
            $this->response['errors'][] = 'Unexpected request type';
            return false;
        }

        return true;
    }

    public function init(): bool
    {
        $this->memberId = $this->request->get('member_id');
        if (empty($this->memberId)) {
            $this->response['status'] = 'error';
            $this->response['errors'][] = 'Empty member id';

            return false;
        }

        $this->storage = new ConsumerStorage();
        $this->consumerOptions = $this->storage->getOptions($this->memberId);

        if (empty($this->consumerOptions)) {
            $this->response['status'] = 'error';
            $this->response['errors'][] = 'Specified member id not found #ONUB9j_B0hz3';

            return false;
        }

        return true;
    }

    public function getKey2Dec(): ?string
    {
        return $this->storage->getKey2Dec($this->memberId);
    }

    public function getRequestString(string $name): ?string
    {
        return $this->request->get($name);
    }

    public function getSalt(): ?string
    {
        return $this->consumerOptions['salt'] ?: null;
    }

    public function getRequestAttachments(): array
    {
        return $this->attachments ?: [];
    }

    public function attachIsValid(&$requestAttach, string $hashExpected): bool
    {
        // this version of script doesn't support file interaction
        return false;
        // return \md5_file($requestAttach->path()) == $hashExpected;
    }
}
