<?php

namespace Webformat\FormTransmitter\Http;

use Webformat\FormTransmitter\ConsumerStorage;
use Webformat\FormTransmitter\Options;
use Webformat\Http\SignedInteraction\Send\Kernel;

defined('B_PROLOG_INCLUDED') or exit('no prolog in '.\basename(__FILE__).'!');

class BitrixSendKernel extends Kernel
{
    protected $options;
    protected $consumerOptions;
    protected $consumerStorage;
    protected $storage;

    public function __construct(array $runtimeOpts)
    {
        if (!\Bitrix\Main\Loader::includeModule('form')) {
            throw new \Exception('Module "form" is not installed!');
        }
        parent::{__FUNCTION__}($runtimeOpts);

        if (empty($this->runtime['member_id'])) {
            throw new \Exception('Member id is empty, the request can\'t be sent!');
        }
        $this->storage = new ConsumerStorage();
        $this->consumerOptions = $this->storage->getOptions($this->runtime['member_id']);

        $this->options = Options::get();
        if (empty($this->options['app_host']) || empty($this->options['receive_endpoint'])) {
            throw new \Exception('Empty exchange endpoint!');
        }

        $_this = &$this;
        $this->addCallback('about_to_send', function (array &$request) use (&$_this) {
            $request += [
                'member_id' => $_this->runtime['member_id'],
                'donor_host_id' => $_this->consumerOptions['my_host_id'],
            ];
        });
    }

    public function getEndpoint(): string
    {
        return \rtrim($this->options['app_host'], '/').'/'.\ltrim($this->options['receive_endpoint'], '/');
    }

    public function getKey2Enc(): ?string
    {
        return $this->storage->getKey2Enc($this->runtime['member_id']);
    }

    public function getSalt(): ?string
    {
        return $this->consumerOptions['salt'] ?: null;
    }

    public function post(string $url, array $data, array $attachments, &$responseCode): ?string
    {
        $boundary = \md5(\time());
        $header .= 'Content-Type: multipart/form-data; boundary='.$boundary."\r\n";
        $boundary = '--'.$boundary;

        $header .= "User-Agent: Bitrix Site Management system\r\n";
        $header .= "Accept: */*\r\n";
        $header .= "Connection: close\r\n";

        $content = $this->getFormBody($data, $boundary);
        $content .= $this->getFilesBody($attachments, $boundary);
        $content .= $boundary."--\r\n"; //finalize body

        $context = \stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' => $header,
                'ignore_errors' => true,
                'content' => $content,
            ],
            // 'ssl' => ['verify_peer' => false, 'verify_peer_name' => false],
        ]);

        $start = \microtime(true);
        $response = \file_get_contents($url, false, $context);
        $duration = \round(\microtime(true) - $start, 4);
        $responseCode = $this->getResponseCode($http_response_header);

        return (false === $response) ? null : $response;
    }

    protected function getFilesBody(array $attachments, string $boundary)
    {
        $content = '';
        foreach ($attachments as $index => $finfo) {
            $title = empty($finfo['title']) ? \basename($finfo['path']) : $finfo['title'];
            $mime = \mime_content_type($finfo['path']);
            $fileContents = \file_get_contents($finfo['path']);
            $dataLength = \mb_strlen($fileContents, '8bit');
            $content .= \sprintf(
                "%s\r\n".
                "Content-Disposition: form-data; name=\"attachments[%d]\"; filename=\"%s\"\r\n".
                "Content-Type: %s\r\n".
                "Content-Length: %d\r\n\r\n".
                "%s\r\n",
                $boundary, $index, $title, $mime, $dataLength, $fileContents
            );
        }

        return $content;
    }

    protected function getFormBody(array $data, string $boundary): string
    {
        $data = \http_build_query($data);
        $data = \array_map('urldecode', \explode('&', $data));
        $content = '';
        foreach ($data as $queryPair) {
            if (!(bool) \preg_match('/^(?<name>[^=]+)=(?<value>.*)$/u', $queryPair, $matches)) {
                continue;
            }
            $content .= $boundary."\r\n".
            'Content-Disposition: form-data; name="'.$matches['name'].'"'."\r\n\r\n".
            $matches['value']."\r\n";
        }

        return $content;
    }

    protected function getResponseCode(?array $responseHeaders): int
    {
        if (!\is_null($responseHeaders) && (bool) \preg_match('#HTTP/[\d\.]+\s+(?<code>\d+)#', $responseHeaders[0], $matches)) {
            return (int) $matches['code'];
        }

        return 0;
    }
}
