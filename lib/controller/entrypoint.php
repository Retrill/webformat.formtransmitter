<?php
// https://.../bitrix/services/main/ajax.php?action=webformat:formtransmitter.entrypoint.receive

namespace Webformat\FormTransmitter\Controller;


use Webformat\FormTransmitter\Options;
use Bitrix\Main\Engine\ActionFilter\Authentication;
use Bitrix\Main\Engine\ActionFilter\HttpMethod;
use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;
use Webformat\FormTransmitter\Http\BitrixReceiveKernel as ReceiveKernel;
use Webformat\Http\SignedInteraction\Receive\Director as ReceiveDirector;
use Bitrix\Main\Application;

defined('B_PROLOG_INCLUDED') or exit('no prolog!');
Loc::loadMessages(__FILE__);

// \Bitrix\Main\Loader::includeModule('webformat.utils');

class EntryPoint extends \Bitrix\Main\Engine\Controller
{
    public function receiveAction(string $payload): ?array
    {
        $request = Application::getInstance()->getContext()->getRequest();
        $results = (new ReceiveDirector(new ReceiveKernel($request)))->process();
        if(!empty($results['errors'])){
            foreach($results['errors'] as $errorMsg){
                $errorCode = 0;
                if((bool)\preg_match('/\#(?<code>\b[\w\d_]+)\b/', $errorMsg, $matches)){
                    $errorCode = $matches['code'];
                }
                $this->addError(new Error($errorMsg, $errorCode));
            }
        }

        return isset($results['data']) ? $results['data'] : null;
    }

    protected function getDefaultPreFilters()
    {
        return [
            /*
            new Authentication(),
            new HttpMethod(
                [HttpMethod::METHOD_GET, HttpMethod::METHOD_POST]
            ),
            new ActionFilter\Csrf(), 
            */
        ];
    }

}
