<?php
// https://.../bitrix/services/main/ajax.php?action=webformat:formtransmitter.app.install

namespace Webformat\FormTransmitter\Controller;

use Webformat\FormTransmitter\RestClient;
use Webformat\FormTransmitter\Options;
use Bitrix\Main\Engine\ActionFilter\Authentication;
use Bitrix\Main\Engine\ActionFilter\HttpMethod;
use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;

// use Bitrix\Main\Engine\Response\{AjaxJson, Json};

// use Bitrix\Main\Engine\Controller;
defined('B_PROLOG_INCLUDED') or exit('no prolog!');
Loc::loadMessages(__FILE__);

\Bitrix\Main\Loader::includeModule('webformat.utils');

class App extends \Bitrix\Main\Engine\Controller
{
    protected $connection;
    protected $sortReplacements = [];

    protected function init()
    {
        parent::{__FUNCTION__}();
        $this->connection = \Bitrix\Main\Application::getConnection();
        $this->sqlHelper = $this->connection->getSqlHelper();
    }

    public function installAction(array $credentials): ?array
    {
        $options = Options::get();

        $endpoint = [
            'url' => 'https://'.\rtrim($credentials['domain'], '/').'/rest/',
            'id' => '',
            'secret' => '',
            'confirmed' => false
        ];
        if(!\array_key_exists($credentials['member_id'], $options['endpoints'])){
            $options['endpoints'][$credentials['member_id']] = $endpoint;
            Options::save($options);

            $restClient = new RestClient();
            $restClient->setCredentialsDir(\dirname(__DIR__, 2).'/credentials');
            try {
                $restClient->saveCredentials($credentials['member_id'], $credentials);
            } catch ( \Exception $er ){
                $this->addError(new Error($er->getMessage(), 1));

                return null;
            }
        }

        return [];
    }

    protected function getDefaultPreFilters()
    {
        return [
            // new Authentication(),
            /* new HttpMethod(
                [HttpMethod::METHOD_GET, HttpMethod::METHOD_POST]
            ), */
            // new ActionFilter\Csrf(),
        ];
    }
}
