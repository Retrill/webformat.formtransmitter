<?php
namespace Webformat\FormTransmitter\Controller\Traits;
use Webformat\FormTransmitter\Options;
use Webformat\FormTransmitter\RestClient;
use Bitrix\Main\Localization\Loc;


defined('B_PROLOG_INCLUDED') or exit('no prolog!');
Loc::loadMessages(__FILE__);

\Bitrix\Main\Loader::includeModule('webformat.utils');

trait Restable
{
    protected $restClient;

    protected function getConfirmedEndpoint(array $credentials): ?array{
        //check domain name;
        $requestDomain = $credentials['domain'] ?: '';
        $requestMemberId = $credentials['member_id'] ?: '';
        if (!$requestDomain) {
            return null;
        }
        $options = Options::get();

        if(empty($options['endpoints'] || !\is_array($options['endpoints']))){
            return null;
        }

        foreach($options['endpoints'] as $memberId => $endpoint){
            if(empty($endpoint['confirmed'])){
                continue;
            }
            if(!(bool)\preg_match('/^https?:\/\/(?<domain>[^\/]+)(\/|$)/i', $endpoint['url'], $matches)){
                continue;
            }
            $endpointDomain = $matches['domain'];

            if($requestMemberId == $memberId && $requestDomain == $endpointDomain){
                return $endpoint + ['member_id' => $memberId];
            }
        }

        // на будущее: здесь мы также должны проверить, что verify code из опций модуля для данного
        // endpoint соответствует тому, что сохранён в Б24 (при инсталляции приложения - это тоже надо дописать)

        return null;
    }

    protected function initRestClient(array $endpoint){
        $this->restClient = new RestClient();
        $this->restClient->setCredentialsDir(\dirname(__DIR__, 3).'/credentials');
        $this->restClient->setEndpoint($endpoint['url'], $endpoint['member_id'], $endpoint['id'], $endpoint['secret']);

    }
}
