<?php
// https://.../bitrix/services/main/ajax.php?action=webformat:formtransmitter.app.install

namespace Webformat\FormTransmitter\Controller;


use Webformat\FormTransmitter\Options;
use Bitrix\Main\Engine\ActionFilter\Authentication;
use Bitrix\Main\Engine\ActionFilter\HttpMethod;
use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;

// use Bitrix\Main\Engine\Response\{AjaxJson, Json};

// use Bitrix\Main\Engine\Controller;
defined('B_PROLOG_INCLUDED') or exit('no prolog!');
Loc::loadMessages(__FILE__);

\Bitrix\Main\Loader::includeModule('webformat.utils');

class Webform extends \Bitrix\Main\Engine\Controller
{
    use Traits\Restable;

    protected $connection;
    protected $sortReplacements = [];

    protected function init()
    {
        parent::{__FUNCTION__}();
        $this->connection = \Bitrix\Main\Application::getConnection();
        $this->sqlHelper = $this->connection->getSqlHelper();

        if(!\Bitrix\Main\Loader::includeModule('form')){
            $this->addError(new Error('No "form" module installed!'), 3);
        }
    }

    public function indexAction(array $credentials): ?array
    {
        $options = Options::get();

        if ($endpoint = $this->getConfirmedEndpoint($credentials)) {
            $this->initRestClient($endpoint);
        }else{
            $this->addError(new Error('Unconfirmed'), 1);
            return [];
        }

        if(\current($this->restClient->exec('user.admin'))['result'] !== true){
            $this->addError(new Error('Unauthorized'), 2);
            return [];
        }


        $res = \CForm::getList($by = 'id', $order = 'asc', [], $isFiltered = null);
        $forms = [];
        while($form = $res->fetch()){
            $forms[] = ['id' => (int)$form['ID'], 'name' => \trim($form['NAME'])];
        }
        return $forms;
    }

    public function getSpecificationAction(array $credentials): ?array
    {
        if (!($endpoint = $this->getConfirmedEndpoint($credentials))) {
            $this->addError(new Error('Unconfirmed'), 1);
            return [];
        }

        $specification = [];
        $res = \CForm::getList($by = 'id', $order = 'asc', [], $isFiltered = null);
        while($form = $res->fetch()){
            $specification[$form['ID']] = [];
            $questionRes = \CFormField::getList(
                $form['ID'], 'N', $by = 's_id', $order='desc',
                ['ACTIVE' => 'Y'], $isFiltered
            );
            while($question = $questionRes->fetch()){
                $answer = \CFormAnswer::getList(
                    $question['ID'], $by = 's_sort', $order='asc', [], $isFiltered
                )->fetch();
                $fieldCode = \mb_strtolower($question['SID'], 'UTF-8');
                $specification[$form['ID']][$fieldCode] = [
                    'type' => $answer['FIELD_TYPE'],
                    'title' => $question['TITLE'],
                    'code' => $fieldCode
                ];
            }
        }

        return $specification;
    }

    protected function getDefaultPreFilters()
    {
        return [
            // new Authentication(),
            /* new HttpMethod(
                [HttpMethod::METHOD_GET, HttpMethod::METHOD_POST]
            ), */
            // new ActionFilter\Csrf(),
        ];
    }

}
