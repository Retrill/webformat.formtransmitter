<?php
namespace Webformat\FormTransmitter\Controller;

use Webformat\FormTransmitter\Options as BaseOptions;
use Bitrix\Main\Engine\ActionFilter\{ Authentication, HttpMethod};

use Bitrix\Main\Error;
use Bitrix\Main\Localization\Loc;

defined('B_PROLOG_INCLUDED') or exit('no prolog!');
Loc::loadMessages(__FILE__);

\Bitrix\Main\Loader::includeModule('webformat.utils');

class Options extends \Bitrix\Main\Engine\Controller
{
    use Traits\Restable;

    protected $connection;
    protected $sortReplacements = [];

    protected function init()
    {
        parent::{__FUNCTION__}();
        $this->connection = \Bitrix\Main\Application::getConnection();
        $this->sqlHelper = $this->connection->getSqlHelper();
    }

    public function getMatchesAction(array $credentials): ?array
    {
        if (!($endpoint = $this->getConfirmedEndpoint($credentials))) {
            $this->addError(new Error('Unconfirmed'), 1);
            return [];
        }

        return BaseOptions::get('matches-'.$endpoint['member_id'], true);
    }

    public function saveMatchesAction(array $credentials, array $matches): ?array
    {
        if (!($endpoint = $this->getConfirmedEndpoint($credentials))) {
            $this->addError(new Error('Unconfirmed'), 1);
            return [];
        }

        try{
            BaseOptions::save($matches, 'matches-'.$endpoint['member_id']);
        }catch(\Exception $er){
            $this->addError(new Error($er->getMessage()), 2);
        }

        return [];
    }

    protected function getDefaultPreFilters()
    {
        return [
            // new Authentication(),
            /* new HttpMethod(
                [HttpMethod::METHOD_GET, HttpMethod::METHOD_POST]
            ), */
            // new ActionFilter\Csrf(),
        ];
    }

}
