<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);
\Bitrix\Main\UI\Extension::load('ui.vue');

$langPrefix = 'WEBFORMAT_FORMTRANSMITTER_UNINST_';
?>
<form action="" method="POST" @submit="checksubmit" id="wf_unstep">
<?=bitrix_sessid_post()?>
    <input type="hidden" name="lang" value="<?=\LANGUAGE_ID?>"/>
    <input type="hidden" name="id" value="webformat.formtransmitter"/>
    <input type="hidden" name="uninstall" value="Y"/>
    <input type="hidden" name="step" value="2"/>
    <p><?=Loc::getMessage($langPrefix.'INFO')?></p>
    <p>
    	<input type="checkbox" name="saveopts" id="saveopts" v-model="saveopts"/>
    	<label for="saveopts"><?=Loc::getMessage($langPrefix.'SAVE_OPTS')?></label>
    </p>
    <p>
    	<input type="checkbox" name="savetables" id="savetables" v-model="savetables"/>
    	<label for="savetables"><?=Loc::getMessage($langPrefix.'SAVE_TABLES')?></label>
    </p>
    <input type="submit" name="inst" value="<?=Loc::getMessage($langPrefix.'DEL')?>">
</form>

<script>
BX.Vue.create({
    el: '#wf_unstep',
    data: {
        saveopts: true,
        savetables: true
    },
    methods: {
        checksubmit: function(e){
            if(!this.savetables && !confirm('Do you REALLY want to remove tables?')){
                e.preventDefault();
                return false;
            }
            return true;
        }
    }
});
</script>
