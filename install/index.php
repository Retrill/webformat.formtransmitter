<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (class_exists('webformat_formtransmitter')) {
    return;
}

if (!class_exists('webformat_formtransmitter')) {
    class webformat_formtransmitter extends \CModule
    {
        public const LANG_PREFIX = 'WEBFORMAT_FORMTRANSMITTER_';
        public $MODULE_ID = 'webformat.formtransmitter';
        public $MODULE_VERSION;
        public $MODULE_VERSION_DATE;
        public $MODULE_NAME;
        public $MODULE_DESCRIPTION;
        public $PARTNER_NAME;
        public $PARTNER_URI;
        public $MODULE_GROUP_RIGHTS;
        protected $SITE_ROOT;
        protected $CORE_DIR;
        protected $MODULE_ROOT;
        protected $listeners = [];
        protected $listenersObsolete = [];

        public function __construct()
        {
            $arModuleVersion = [];
            include dirname(__FILE__).'/version.php';
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
            $this->MODULE_NAME = Loc::getMessage(self::LANG_PREFIX.'MODULE_NAME');
            $this->MODULE_DESCRIPTION = Loc::getMessage(self::LANG_PREFIX.'MODULE_DESC');
            $this->MODULE_GROUP_RIGHTS = 'Y';

            $this->PARTNER_NAME = Loc::getMessage(self::LANG_PREFIX.'PARTNER_NAME');
            $this->PARTNER_URI = 'https://www.webformat.ru';
            $this->SITE_ROOT = rtrim($_SERVER['DOCUMENT_ROOT'], '/').'/';
            $this->CORE_DIR = is_dir($this->SITE_ROOT.'local/modules/'.$this->MODULE_ID) ? 'local' : 'bitrix';
            $this->MODULE_ROOT = $this->SITE_ROOT.$this->CORE_DIR.'/modules/'.$this->MODULE_ID.'/';
            $this->phpRoot = rtrim($_SERVER['DOCUMENT_ROOT'], '/').'/';

            if (!class_exists('Webformat\FormTransmitter\Options')) {
                include $this->MODULE_ROOT.'lib/options.php';
            }
            $this->listeners = [
            /*
                array(
                    'catalog',
                    'OnSuccessCatalogImport1C',
                    array(
                        'Webformat\FormTransmitter\Listeners\Catalog',
                        'updateFormTransmitter'
                    )
                )
            */
            ];

            $this->listenersObsolete = [
                [
                    'main',
                    'OnEpilog',
                    $this->MODULE_ID,
                    'Webformat\\FormTransmitter\\Listeners\\Main',
                    'wfOnEpilog',
                ],
                [
                    'form',
                    'onAfterResultAdd',
                    $this->MODULE_ID,
                    'Webformat\\FormTransmitter\\Listeners\\Form',
                    'wfOnAfterResultAdd',
                ],
            ];
        }

        public function InstallDB($arParams = [])
        {
            $conn = \Bitrix\Main\Application::getConnection();
            $dbType = strtolower($conn->getType());

            global $DB, $DBType;
            $sqlFile = $this->MODULE_ROOT.'install/db/'.$dbType.'/install.sql';
            if (!file_exists($sqlFile)) {
                return true;
            }
            if (file_exists($sqlFile) && ($errors = $conn->executeSqlBatch($sqlFile))) {
                throw new \Exception(Loc::getMessage(self::LANG_PREFIX.'SQL_FILE_FAILURE').'<br />'.var_export($errors, true));
            }

            $sqlFile = $this->MODULE_ROOT.'install/db/'.$dbType.'/install$$.sql';
            if (file_exists($sqlFile) && ($queries = explode('$$', trim(file_get_contents($sqlFile))))) {
                $errors = [];
                foreach ($queries as $query) {
                    try {
                        if (!$query = trim($query)) {
                            continue;
                        }
                        $conn->query($query);
                    } catch (\Bitrix\Main\SqlException $ex) {
                        $errors[] = htmlspecialcharsbx($ex->getMessage());
                    }
                }
                if ((bool) $errors) {
                    throw new \Exception(Loc::getMessage(self::LANG_PREFIX.'SQL_FILE_FAILURE').'<br />'.var_export($errors, true));
                }
            }

            return true;
        }

        public function UnInstallDB($arParams = [])
        {
            return true;
        }

        public function InstallEventListeners()
        {
            $em = \Bitrix\Main\EventManager::getInstance();
            foreach ($this->listeners as $listener) {
                call_user_func_array([$em, 'registerEventHandler'], $listener);
            }
            foreach ($this->listenersObsolete as $listener) {
                call_user_func_array('RegisterModuleDependences', $listener);
            }
        }

        public function UnInstallEventListeners()
        {
            $em = \Bitrix\Main\EventManager::getInstance();
            foreach ($this->listeners as $listener) {
                call_user_func_array([$em, 'unregisterEventHandler'], $listener);
            }
            foreach ($this->listenersObsolete as $listener) {
                call_user_func_array('UnRegisterModuleDependences', $listener);
            }
        }

        public function InstallFiles()
        {
            return true;
        }

        public function UnInstallFiles()
        {
            return true;
        }

        protected function resetOptions()
        {
            return \Webformat\FormTransmitter\Options::reset();
        }

        public function DoInstall()
        {
            global $APPLICATION, $step;
            if ($APPLICATION->GetGroupRight('main') < 'W') {
                return false;
            }
            try {
                $step = (int) $step;
                /*if($step < 2){
                    $APPLICATION->IncludeAdminFile(
                        Loc::getMessage(self::LANG_PREFIX.'STEP1'),
                        $this->MODULE_ROOT.'install/step-01.php'
                    );
                    return true;
                }*/
                $this->InstallFiles();
                $this->InstallDB();
                \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
                $this->InstallEventListeners();
                $APPLICATION->IncludeAdminFile(
                    Loc::getMessage(self::LANG_PREFIX.'FINISH_TITLE'),
                    $this->MODULE_ROOT.'install/finish.php'
                );
                //CAgent::AddAgent("CItrackWebmessenger::NewMessNotifyAgent();", $this->MODULE_ID, "N", 1800);
            } catch (\Throwable $er) {
                $APPLICATION->ThrowException($er->getMessage());

                return false;
            } catch (\Exception $er) {
                $APPLICATION->ThrowException($er->getMessage());

                return false;
            }

            return true;
        }

        public function DoUninstall()
        {
            global $APPLICATION, $step;
            if ($GLOBALS['APPLICATION']->GetGroupRight('main') < 'W') {
                return;
            }
            try {
                $step = (int) $step;
                if ($step < 2) {
                    $this->UnInstallFiles();
                    $APPLICATION->IncludeAdminFile(
                        Loc::getMessage($this->MODULE_ID.'_TITLE'),
                        $this->MODULE_ROOT.'install/unstep.php'
                    );
                } elseif (2 == $step) {
                    if (empty($_REQUEST['saveopts'])) {
                        if (!$this->resetOptions()) {
                            return false;
                        }
                    }
                    if (empty($_REQUEST['savetables'])) {
                        $this->UnInstallDB();
                    }
                    \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);
                    $APPLICATION->IncludeAdminFile(
                        Loc::getMessage(self::LANG_PREFIX.'UNFINISH_TITLE'),
                        $this->MODULE_ROOT.'install/unfinish.php'
                    );
                } else {
                    throw new \Exception('Unexpected installation step!');
                }
            } catch (\Exception $er) {
                $APPLICATION->ThrowException($er->getMessage());

                return false;
            }

            return true;
        }
    }
}
