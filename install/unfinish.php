<?
if(!check_bitrix_sessid()){return;}
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$langPrefix = 'WEBFORMAT_FORMTRANSMITTER_UNINST_';
$notes = array();
if(empty($_REQUEST['saveopts'])){
    $notes[] = Loc::getMessage($langPrefix.'OPTS_DELETED');
}else{
    $notes[] = Loc::getMessage($langPrefix.'OPTS_SAVED');
}

if(empty($_REQUEST['savetables'])){
    $notes[] = Loc::getMessage($langPrefix.'TABLES_DELETED');
}else{
    $notes[] = Loc::getMessage($langPrefix.'TABLES_SAVED');
}

if(empty($errors)){
    $notes[] = Loc::getMessage($langPrefix.'RESULT_OK'); 
}

if((bool)$notes){
    echo \CAdminMessage::ShowNote(implode('<br />', $notes));
}
if((bool)$errors){
    echo \CAdminMessage::ShowMessage(array('TYPE'=>'ERROR', 'MESSAGE' => Loc::getMessage('MOD_UNINST_ERR'), 'DETAILS' => implode('<br />', $errors), 'HTML' => true));
}
?>
<form action="">
    <input type="hidden" name="lang" value="<?=\LANG?>"/>
    <input type="submit" name="" value="<?=Loc::getMessage('MOD_BACK');?>"/>    
</form>
