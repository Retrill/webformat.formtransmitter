<?php

namespace Webformat\FormTransmitter\Tabs\Basic;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Webformat\FormTransmitter\ConsumerStorage;

Loc::loadMessages(__FILE__);
$langPrefixTab = 'WEBFORMAT_FORMTRANSMITTER_TAB_BASIC_';

$storage = new ConsumerStorage();
\CJSCore::init(['ajax']);
$checkUrl = '/'.\trim(\mb_substr(\dirname(__DIR__), \mb_strlen(Loader::getDocumentRoot(), 'UTF-8'), null, 'UTF-8'), '/').'/';
$checkUrl .= 'http/check.php';
?>
<tr>
    <td><?php echo Loc::getMessage($langPrefixTab.'CHECK_CONNECTION'); ?>:
    </td>
    <td>
        <select name="webformat[formtransmitter][check_conn]" size="1" id="b24_member_id">
            <?php
                foreach ($storage->list() as $memberId) {
                    $memberOpts = $storage->getOptions($memberId);
                    echo '<option value="', $memberId,'">', $memberOpts['server_name'], '</option>';
                }
            ?>
        </select>
        <button id="checkConnection"><?php echo Loc::getMessage($langPrefixTab.'CHECK_CONNECTION_BTN'); ?></button>
        <div id="check_results" class="check-results"></div>
        <br /><span class="subtitle">(<?php echo Loc::getMessage($langPrefixTab.'CHECK_CONNECTION_SUBTITLE'); ?>)</span>
    </td>
</tr>

<script>
    (function() {
        BX.bind(BX('checkConnection'), 'click', function(e) {
            e.preventDefault();
            var memberId = BX('b24_member_id').value;
            var results = BX('check_results');
            results.innerText = '...';

            BX.ajax.post(
                '<?php echo $checkUrl; ?>', {
                    'member_id': memberId
                },
                function(response) {
                    response = JSON.parse(response);
                    console.log('response: ', response);

                    if (response.status == 'ok') {
                        results.innerText = 'OK';
                        BX.removeClass(results, 'error');
                        BX.addClass(results, 'success');
                    } else {
                        results.innerText = 'Error! ' + response.errors.join(' / ');
                        console.error('Connection errors: ', response.errors);
                        BX.removeClass(results, 'success');
                        BX.addClass(results, 'error');
                    }
                }
            );

        });
    })();
</script>