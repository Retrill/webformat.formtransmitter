import Leads from './components/leads.vue';

export default [
    { path: '/', redirect: '/leads'},
    /* {
        path: '/general',
        component: Leads
    }, */
    {
        // path: '/:pathMatch(.*)*',
        // path: '/:pathMatch(leads|)*',
        path: '/leads',
        component: Leads
    },
];
