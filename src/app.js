import { createApp } from 'vue';
import * as VueRouter from 'vue-router';
import routes from './routes.js';
import App from './app.vue';

import '@imengyu/vue3-context-menu/lib/vue3-context-menu.css';
import ContextMenu from '@imengyu/vue3-context-menu';

/* console.log('hash: ', location.hash);
if (location.hash) {
    location.replace(location.hash.replace('#', ''));
} */

const router = VueRouter.createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    // history: VueRouter.createWebHistory(),
    history: VueRouter.createWebHashHistory(),
    base:"/local/modules/webformat.formtransmitter/view/app.php",
    routes,
});

BX24.init(function(){
    createApp(App)
        .use(router)
        .use(ContextMenu)
        .mount('#app');
});
