<?php
// require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
// require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
require_once(rtrim($_SERVER['DOCUMENT_ROOT'],'/').'/bitrix/modules/main/include/prolog_before.php');
$APPLICATION->restartBuffer();
\CJSCore::init(['popup']);
?><!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Приёмник вебформ</title>
    <?=$APPLICATION->showHead();?>
</head>
<body>
    <div id="app">
        <app/>
    </div>
    <script src="//code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="//api.bitrix24.com/api/v1/"></script>
    <script src="../assets/js/bundle.js?<?php echo \mt_rand(1, \mt_getrandmax()); ?>"></script>
</body>
</html>
