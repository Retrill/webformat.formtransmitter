<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Установка</title>
    <link rel="stylesheet" href="/local/modules/webformat.formtransmitter/assets/css/install.css"/>
</head>
<body>
    <div class="card">
        <h1>Приёмник вебформ с сайта</h1>
        <hr />
        <p v-if="awaiting">Идёт установка...</p>
        <p class="loading"></p>
        <div class="report"></div>
        <button class="btn continue">OK</button>
    </div>
<?php /*/?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/qs/6.10.1/qs.js"></script>
<?php /*/?>
    <script src="//code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="//api.bitrix24.com/api/v1/"></script>
    <script src="/local/modules/webformat.formtransmitter/assets/js/install.js"></script>
</body>
</html>