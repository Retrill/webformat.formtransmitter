<?php

return [
    'app_host' => 'https://forms.webformat.market',
    'handshake_endpoint' => '/hosts/#host_id#',
    'receive_endpoint' => '/receive',
];
