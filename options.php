<?php

namespace Webformat\FormTransmitter;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

global $APPLICATION;
\Bitrix\Main\Loader::includeModule('webformat.formtransmitter');

$langPrefix = 'WEBFORMAT_FORMTRANSMITTER_OPTIONS_';
Loc::loadMessages(__FILE__);

$APPLICATION->setTitle(Loc::getMessage($langPrefix.'PAGE_TITLE_POST'));

if (!empty($_REQUEST['handshake'])) {
    $connector = new Connector();
    if (!$connector->handshake($_REQUEST)) {
        echo (new \CAdminMessage(\implode('<br>', $connector->getErrors())))->show();
    } else {
        echo \CAdminMessage::showNote(Loc::getMessage($langPrefix.'HANDSHAKE_OK'));

        $helloResults = $connector->sayHello($_REQUEST['member_id']);
        if ('ok' == $helloResults['status']) {
            echo \CAdminMessage::showNote(Loc::getMessage($langPrefix.'HELLO_OK'));
        } else {
            echo (new \CAdminMessage(Loc::getMessage($langPrefix.'HELLO_ERROR').' '.\implode('<br>', $helloResults['errors'])))->show();
        }
    }

    return;
}

$css = rtrim(__DIR__, '/').'/assets/css/tabs.css';
$asset = \Bitrix\Main\Page\Asset::getInstance();
$asset->addString('<style>'.file_get_contents($css).'</style>');
/*
$css = rtrim(__DIR__, '/').'/assets/lib/jquery.multiinput/style.css';
$asset->addString('<style>'.file_get_contents($css).'</style>');

$assetPrefix = substr(rtrim(__DIR__, '/'), strlen(rtrim($_SERVER['DOCUMENT_ROOT'], '/')));
$asset->addJs($assetPrefix.'/assets/js/jquery.js');
$asset->addJs($assetPrefix.'/assets/lib/jquery.multiinput/script.js');
*/

if (!Loader::includeModule('webformat.formtransmitter')) {
    echo \CAdminMessage::ShowMessage([
        'TYPE' => 'ERROR',
        'MESSAGE' => Loc::getMessage($langPrefix.'NEED_REQUIRED_MODULES'),
    ]);

    return;
}

// get user access level to this module
$POST_RIGHT = $APPLICATION->GetGroupRight('webformat.formtransmitter');
// show authorization form if no access
if ('D' == $POST_RIGHT) {
    $APPLICATION->AuthForm(Loc::getMessage($langPrefix.'ACCESS_DENIED'));
}

if (isset($_REQUEST['webformat']['formtransmitter']['bsend'])) {
    unset($_REQUEST['webformat']['formtransmitter']['bsend']);
    if (empty($_REQUEST['webformat']['formtransmitter'])) {
        echo \CAdminMessage::ShowMessage([
            'TYPE' => 'ERROR',
            'MESSAGE' => Loc::getMessage($langPrefix.'EMPTY_OPTIONS'),
        ]);
    }
    if (Options::Save($_REQUEST['webformat']['formtransmitter'])) {
        echo \CAdminMessage::ShowNote(Loc::getMessage($langPrefix.'OPTIONS_SAVED'));
    }
}

//Tabs description
$aTabs = [];
$tabFiles = glob(rtrim(__DIR__, '/').'/option_tabs/{[^_],.}*.php', GLOB_BRACE);
foreach ($tabFiles as $filePath) {
    $basename = basename($filePath);
    $label = strtolower(substr($basename, 0, strrpos($basename, '.')));
    if (preg_match('#^\d+\-(.*)$#', $label, $matches)) {
        $label = $matches[1];
    }
    $aTabs[] = [
        'DIV' => 'webformat_formtransmitter_'.$label,
        'TAB' => Loc::getMessage($langPrefix.'TAB_'.strtoupper($label)),
        'ICON' => '',
        'TITLE' => Loc::getMessage($langPrefix.'TAB_'.strtoupper($label).'_TITLE'),
        'FILE' => $basename,
    ];
}

//Initialazing tabs
$oTabControl = new \CAdmintabControl('tabControl', $aTabs);
$oTabControl->Begin();
$options = Options::get();
?>
<form method="post"
    action="<?php echo $APPLICATION->GetCurPage(); ?>?mid=webformat.formtransmitter&lang=<?php echo \LANG; ?>&mid_menu=1"
    class="webformat-formtransmitter">
    <?php echo bitrix_sessid_post(); ?>
    <?php
        foreach ($aTabs as $tab) {
            $oTabControl->BeginNextTab();
            include __DIR__.'/option_tabs/'.$tab['FILE'];
        }
    ?>

    <?$oTabControl->Buttons();?>
    <input type="submit" name="webformat[formtransmitter][bsend]"
        value="<?php echo Loc::getMessage($langPrefix.'BSEND'); ?>" />
    <?$oTabControl->End();?>
</form>