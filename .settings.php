<?php
return [
    'controllers' => [
        'value' => [
            'defaultNamespace' => '\\Webformat\\FormTransmitter\\Controller',
        ],
        'readonly' => true,
    ]
];
