<?php

$langPrefix = 'WEBFORMAT_FORMTRANSMITTER_OPTIONS_';
$MESS[$langPrefix.'PAGE_TITLE_GET'] = 'Настройки модуля!';
$MESS[$langPrefix.'PAGE_TITLE_POST'] = 'Подтверждение заявки';
$MESS[$langPrefix.'NEED_REQUIRED_MODULES'] = 'Модуль webformat.formtransmitter не установлен!';
$MESS[$langPrefix.'ACCESS_DENIED'] = 'В доступе отказано';
$MESS[$langPrefix.'BSEND'] = 'Сохранить';
$MESS[$langPrefix.'EMPTY_OPTIONS'] = 'Невозможно сохранить настройки, переданы пустые данные';
$MESS[$langPrefix.'TAB_BASIC'] = 'Основные настройки';
$MESS[$langPrefix.'TAB_BASIC_TITLE'] = '';
$MESS[$langPrefix.'OPTIONS_SAVED'] = 'Настройки успешно сохранены!';
$MESS[$langPrefix.'HANDSHAKE_OK'] = 'Приложение корректно подключено!';
$MESS[$langPrefix.'HELLO_OK'] = 'Обмен тестовым сообщением успешно произведён!';
$MESS[$langPrefix.'HELLO_ERROR'] = 'Ошибка обмена тестовым сообщением!';
