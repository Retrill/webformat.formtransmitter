<?php

$langPrefix = 'WEBFORMAT_FORMTRANSMITTER_';
$MESS[$langPrefix.'MODULE_NAME'] = 'CRM-вебформы (модуль для Битрикс: Управление Сайтом)';
$MESS[$langPrefix.'MODULE_DESC'] = 'Пересылает данные из форм в Битрикс24 через REST API';
$MESS[$langPrefix.'PARTNER_NAME'] = 'Вебформат';

$MESS[$langPrefix.'FINISH_TITLE'] = 'Установка завершена';
$MESS[$langPrefix.'UNFINISH_TITLE'] = 'Удаление завершено';
$MESS[$langPrefix.'FILES_INIT_WRITE_FAILURE'] = 'Ошибка записи файла init.php';

//$MESS[$langPrefix.'SQL_FILE_NOT_FOUND'] = 'Ошибка обращения к SQL-файлу! Проверьте его существование в папке модуля';
$MESS[$langPrefix.'SQL_FILE_FAILURE'] = 'Ошибка выполнения SQL-файла!';

$MESS[$langPrefix.'DIR_FAILURE'] = 'Не удалось создать папку "#PATH#"! Проверьте linux-права на файлы.';
$MESS[$langPrefix.'LINK_FAILURE'] = 'Не удалось создать символьную ссылку "#PATH#"!';

$MESS[$langPrefix.'UNEXPECTED_OPTIONS'] = 'Неожиданные опции модуля!';
$MESS[$langPrefix.'TARGET_EXISTS'] = 'Папка "#PATH#" уже существует, компонент не может быть установлен! Удалите папку и повторите попытку.';
$MESS[$langPrefix.'WRITE_ERROR'] = 'Не удалось осуществить запись в файл/папку по пути #PATH#!';
$MESS[$langPrefix.'STEP1'] = 'Предварительные настройки';
