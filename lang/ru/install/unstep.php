<?
$langPrefix = 'WEBFORMAT_FORMTRANSMITTER_UNINST_';
$MESS[$langPrefix.'SAVE_OPTS'] = 'Сохранить настройки модуля';
$MESS[$langPrefix.'SAVE_TABLES'] = 'Сохранить таблицы';
$MESS[$langPrefix.'INFO'] = 'Вы можете сохранить файл опций модуля. Они будут использованы при следующих установках.';
$MESS[$langPrefix.'DEL'] = 'Удалить модуль';