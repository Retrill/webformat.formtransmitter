<?
$langPrefix = 'WEBFORMAT_FORMTRANSMITTER_UNINST_';
$MESS[$langPrefix.'OPTS_SAVED'] = 'Настройки модуля сохранены!';
$MESS[$langPrefix.'OPTS_DELETED'] = 'Настройки модуля удалены!';
$MESS[$langPrefix.'TABLES_SAVED'] = 'Таблицы модуля сохранены!';
$MESS[$langPrefix.'TABLES_DELETED'] = 'Таблицы модуля удалены!';
$MESS[$langPrefix.'RESULT_OK'] = 'Модуль успешно удалён!';
