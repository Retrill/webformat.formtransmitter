<?php

$langPrefix = 'WEBFORMAT_FORMTRANSMITTER_TAB_BASIC_';

$MESS[$langPrefix.'PAGE_TITLE'] = 'Первоначальные настройки';
$MESS[$langPrefix.'CHECK_CONNECTION'] = 'Проверить соединение с Битрикс24';
$MESS[$langPrefix.'CHECK_CONNECTION_BTN'] = 'Проверить';
$MESS[$langPrefix.'CHECK_CONNECTION_SUBTITLE'] = 'выберите подключенный Битрикс24 из списка';
