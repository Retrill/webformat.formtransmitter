<?php

$langPrefix = 'WEBFORMAT_FORMTRANSMITTER_OPTIONS_';
$MESS[$langPrefix.'NONEXISTENT_DEFAULT_FILE'] = 'Отсутствует файл с настройками по умолчанию';
$MESS[$langPrefix.'SAVE_FAILURE'] = 'Ошибка сохранения контента файла! Проверьте права доступа к файлу (ошибка вызвана при сохранении опций).';
$MESS[$langPrefix.'UNLINK_FAILURE'] = 'Ошибка удаления файла настроек! Проверьте права доступа к файлу.';
