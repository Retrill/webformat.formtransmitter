<?php

$langPrefix = 'WEBFORMAT_FORMTRANSMITTER_CONNECTOR_';
$MESS[$langPrefix.'NONCANONICAL_CREDENTIALS'] = 'Неверные учётные данные Битрикс24!';
$MESS[$langPrefix.'PKEY_SAVE_ERROR'] = 'Не удалось создать файл приватного ключа!';
$MESS[$langPrefix.'AUTHENTICATION_ERROR'] = 'Ошибка аутентификации на сервере приложения!';
$MESS[$langPrefix.'EMPTY_HOST_ID'] = 'Не указан идентификатор хоста!';
$MESS[$langPrefix.'EMPTY_REPSONSE'] = 'Пустой ответ сервера приложения!';
$MESS[$langPrefix.'PUBLIC_KEY_SAVE_ERROR'] = 'Ошибка сохранения полученного публичного ключа!';
$MESS[$langPrefix.'CONSUMER_OPTIONS_SAVE_ERROR'] = 'Ошибка сохранения настроек для хоста Битрикс24!';
