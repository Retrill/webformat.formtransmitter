<?
declare(strict_types=1);
/*

php /home/bitrix/shared/cli/gate.php -d"/home/bitrix/www" -s"bitrix-dev.brucite.plus" local/modules/webformat.load/bin/import.php

flock -xn /home/bitrix/tmp/example.lock php -n -c "/home/bitrix/cli.php.ini" /home/bitrix/cli/gate.php -d"/home/bitrix/ext_www/example.ru" -s"example.ru" local/modules/webformat.formtransmitter/bin/example.php &> /home/bitrix/log/example.log
*/
\define('NOT_CHECK_PERMISSIONS',true);
require_once(rtrim($_SERVER['DOCUMENT_ROOT'],'/').'/bitrix/modules/main/include/prolog_before.php');

\ini_set('error_reporting', (string)(\E_ALL & ~\E_WARNING & ~\E_NOTICE & ~\E_STRICT & ~\E_DEPRECATED));
\array_map('Bitrix\Main\Loader::includeModule', ['webformat.loadapi', 'webformat.load']);

$controller = (new class() extends \Webformat\LoadApi\Controller {})->setUserLogin('1c-exchange');

// define universal params for all engines
$params = [
    'filter' => [
        'max_filemtime' => \trim(\shell_exec('date +%Y%m%d%H%M%S.%N'))
    ]
];


$multiEngine = new \Webformat\LoadApi\Engine\Composite();

\array_map([$multiEngine, 'add'], [
    new Invoker(new \Webformat\FormTransmitter\Engine\Example(), $params),
]);

$controller->setEngine($multiEngine);
$controller([]);
