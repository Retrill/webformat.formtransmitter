(function(){
    function addTraceTo(form){
        if(!('TRACE' in form)){
            let trace = b24Tracker.guest.getTrace();
            let input = document.createElement('INPUT');
            input.setAttribute('name', 'TRACE');
            input.setAttribute('type', 'hidden');
            input.setAttribute('value', trace);
            form.appendChild(input);
        }
    };
    BX.bindDelegate(document, 'submit', {tagName: 'FORM'}, function(e){
        addTraceTo(e.target);
    });

    // compatibility with aspro webforms
    let formSubmitListener = function(e){
        if(!e.data || !e.data.length || !e.data[0].form){ return; }
        addTraceTo(e.data[0].form);
    }
    BX.Event.EventEmitter.subscribe('onSubmitForm', formSubmitListener);
    BX.Event.EventEmitter.getListeners('onSubmitForm').get(formSubmitListener).sort = 1;
})();
