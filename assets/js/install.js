BX24.init(function(){
    $.post(
        '/bitrix/services/main/ajax.php?action=webformat:formtransmitter.app.install',
        { credentials: BX24.getAuth() },
        function(response){
            console.log('response: ', response);
            if(response.errors.length > 0){
                console.error('Response returned errors: ', response.errors);
            }else if(response.status == 'success'){
                // console.log('success response: ', response);
                BX24.installFinish();
            }
        },
        'json'
    );
});
