<?php

namespace Webformat\FormTransmitter\Http;

use Bitrix\Main\Loader;
use Webformat\FormTransmitter\Connector;

require_once rtrim($_SERVER['DOCUMENT_ROOT'], '/').'/bitrix/modules/main/include/prolog_before.php';
if (empty($USER) || !$USER->isAdmin()) {
    \http_response_code(403);
    exit('access denied');
}
if (empty($_REQUEST['member_id'])) {
    \http_response_code(400);
    exit('wrong member id');
}

Loader::includeModule('webformat.formtransmitter');

class Check
{
    public function __invoke($memberId)
    {
        $connector = new Connector();
        echo \json_encode($connector->sayHello($memberId), \JSON_UNESCAPED_UNICODE);
    }
}

(new Check())($_REQUEST['member_id']);
